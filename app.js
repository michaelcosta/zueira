var express = require('express');
var app = express();
var path = require("path");
var spawn = require('child_process').spawn;

app.use(express.static('static'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, './index.html'));
});

app.post('/brasilsilsil', function(req, res) {
    spawn('mpg123', ['brasilsilsil.mp3']);
    res.sendStatus(200);
});

app.post('/errou', function(req, res) {
    spawn('mpg123', ['errou.mp3']);
    res.sendStatus(200);
});

app.listen(3000, function () {
  console.log('zueira rodando on port 3000!');
});
